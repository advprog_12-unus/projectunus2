[![pipeline status](https://gitlab.com/advprog_12-unus/projectunus/badges/master/pipeline.svg)](https://gitlab.com/advprog_12-unus/projectunus/-/commits/master)
[![coverage report](https://gitlab.com/advprog_12-unus/projectunus/badges/master/coverage.svg)](https://gitlab.com/advprog_12-unus/projectunus/-/commits/master)

Advanced Programming
Kelas A - Kelompok 12

Anggota:
1. Andrew Theodore - 1806133862
2. Evando Wihalim - 1806205445
3. Salman Ahmad N. - 1806204890
4. Insanul Fahmi - 1706979291

Nama Project: Unus

Deskripsi Project:
Aplikasi untuk mengelola keuangan anda. 
Dengan menggunakan aplikasi UNUS, Anda dapat mengelola banyak dompet yang isinya
berupa berbagai jenis uang anda.
#UrusanNgelolaUangjadiSimple


Pembagian Tugas:
1. Andrew Theodore - Dompet
2. Evando Wihalim - Notification & Split Bill
3. Salman Ahmad N. - Account 
4. Insanul Fahmi - Uang 

Design Pattern yang digunakan:
1. Abstract Factory Pattern : Implementasinya dalam pembuatan dompet dan uang.
2. Observer Pattern : Diimplementasikan dalam fitur notifikasi jatuh tempo tagihan.

Implementasi Microservice:
1. Eureka
    a. Gitlab : https://gitlab.com/advprog_12-unus/eurekaunus
    b. Heroku : http://unus-registry.herokuapp.com/

2. Service utama
    a. Gitlab : https://gitlab.com/advprog_12-unus/projectunus2
    b. Heroku : http://unus-a12.herokuapp.com/

3. Service Notes
    a. Gitlab : https://gitlab.com/advprog_12-unus/notesunus
    b. Heroku : http://unus-note.herokuapp.com/
    
4. Service Uangs
    a. Gitlab : https://gitlab.com/advprog_12-unus/uangunus
    b. Heroku : http://unus-uang.herokuapp.com/

Link gitlab kelompok : https://gitlab.com/advprog_12-unus

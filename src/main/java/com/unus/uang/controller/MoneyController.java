package com.unus.uang.controller;

import com.unus.account.repository.UserRepository;
import com.unus.uang.model.UangBase;
import com.unus.uang.service.MoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class MoneyController {
    @Autowired
    private MoneyService moneyService;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RestTemplate restTemplate;

    public MoneyController(MoneyService moneyService) {
        this.moneyService = moneyService;
    }

    long dompetID;
    long userID;

    @GetMapping("/uangs/{idDompet}")
    private String getUang(@PathVariable Long idDompet, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Long idUser = userRepo.findByUserName(auth.getName()).getId();
        dompetID = idDompet;
        userID = idUser;
        model.addAttribute("uangList",
                restTemplate.getForObject(
                        "http://UNUSUANG-SERVICE/service-uangs/allUangs/" + idUser + "/" + idDompet,
                        UangBase[].class));
        return "Money/money";
    }

    @GetMapping("/uangs/create-uang")
    public String createUang(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Long idUser = userRepo.findByUserName(auth.getName()).getId();
        model.addAttribute("newUang",
                restTemplate.getForObject("http://UNUSUANG-SERVICE/service-uangs/create-uang/" + idUser + "/" + dompetID,
                        UangBase.class));
        return "Money/moneyForm";
    }

    @PostMapping("/uangs/add-uang")
    public String addUang(@ModelAttribute("moneys") UangBase moneyEntity) {
        HttpEntity<UangBase> request = new HttpEntity<>(moneyEntity);
        restTemplate.postForObject("http://UNUSUANG-SERVICE/service-uangs/add-uang/" + userID + "/" + dompetID,
                request, UangBase.class);
        return "redirect:/uangs/"+dompetID;
    }

    @GetMapping("/uangs/delete/{id}")
    public String deleteUang(@PathVariable Long id) {
        restTemplate.getForObject("http://UNUSUANG-SERVICE/service-uangs/delete-uang/" + id, void.class);
        return "redirect:/uangs/"+dompetID;
    }

}

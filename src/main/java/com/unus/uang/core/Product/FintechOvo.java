package com.unus.uang.core.Product;

public class FintechOvo implements Money{

    @Override
    public String createMoney() {
        return "Create Ovo";
    }

    @Override
    public String getCode() {
        return "003";
    }

    @Override
    public String getNameFintech() {
        return "OVO";
    }

    @Override
    public int getAmountMoney() {
        return 0;
    }
}

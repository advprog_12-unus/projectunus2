package com.unus.uang.core.Product;

public class FintechDana implements Money {

    @Override
    public String createMoney() {
        return "Create Dana";
    }

    @Override
    public String getCode() {
        return "001";
    }

    @Override
    public String getNameFintech() {
        return "DANA";
    }

    @Override
    public int getAmountMoney() {
        return 0;
    }
}

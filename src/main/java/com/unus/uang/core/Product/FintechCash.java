package com.unus.uang.core.Product;

public class FintechCash implements Money {

    @Override
    public String createMoney() {
        return "Create Cash";
    }

    @Override
    public String getCode() {
        return "000";
    }

    @Override
    public String getNameFintech() {
        return "CASH";
    }

    @Override
    public int getAmountMoney() {
        return 0;
    }
}

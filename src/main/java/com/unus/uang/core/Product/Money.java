package com.unus.uang.core.Product;

public interface Money {
    public String createMoney();
    public String getCode();
    public String getNameFintech();
    public int getAmountMoney();
}

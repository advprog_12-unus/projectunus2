package com.unus.uang.repository;

import com.unus.uang.model.MoneyTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyRepository extends JpaRepository<MoneyTable, Long> {
}

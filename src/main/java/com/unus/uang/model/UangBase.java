package com.unus.uang.model;

public class UangBase {
    private long id;
    private String code;
    private long userid;
    private long dompetid;
    private String nameOfFintech;
    private long amountOfMoney;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameOfFintech() {
        return nameOfFintech;
    }

    public void setNameOfFintech(String name) {
        this.nameOfFintech = name;
    }

    public long getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(long money) {
        this.amountOfMoney = money;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userid;
    }

    public void setUserId(long id_user) {
        this.userid = id_user;
    }

    public long getDompetId() {
        return dompetid;
    }

    public void setDompetId(long id_dompet) {
        this.dompetid = id_dompet;
    }

}

package com.unus.uang.model;

import com.unus.uang.core.MoneyFactory;

import javax.persistence.*;

@Entity
@Table(name = "Money")
public class MoneyTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "code", nullable = true)
    private String code;

    @Column(name = "nameOfFintech", nullable = true)
    private String nameOfFintech;

    @Column(name = "amoutOfMoney", nullable = true)
    private long amountOfMoney;

    @Column(name = "userid", nullable = true)
    private long userid;

    @Column(name = "dompetid", nullable = true)
    private long dompetid;

    public MoneyTable(){}

    public MoneyTable(String nameOfFintech, long amountOfMoney, long userid, long dompetid) {
        this.code = MoneyFactory.getMoney(nameOfFintech).getCode();
        this.nameOfFintech = nameOfFintech;
        this.amountOfMoney = amountOfMoney;
        this.userid = userid;
        this.dompetid = dompetid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameOfFintech() {
        return nameOfFintech;
    }

    public void setNameOfFintech(String name) {
        this.nameOfFintech = name;
    }

    public long getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(long money) {
        this.amountOfMoney = money;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userid;
    }

    public void setUserId(long id_user) {
        this.userid = id_user;
    }

    public long getDompetId() {
        return dompetid;
    }

    public void setDompetId(long id_dompet) {
        this.dompetid = id_dompet;
    }
}

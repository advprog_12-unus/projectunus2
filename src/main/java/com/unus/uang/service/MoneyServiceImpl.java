package com.unus.uang.service;

import com.unus.uang.model.MoneyTable;
import com.unus.uang.repository.MoneyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoneyServiceImpl implements MoneyService {

    public MoneyServiceImpl(MoneyRepository moneyRepository){
        this.moneyRepository = moneyRepository;
    }

    @Autowired
    private MoneyRepository moneyRepository;

    @Override
    public List<MoneyTable> findAll() {
        return moneyRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        moneyRepository.deleteById(id);
    }

    @Override
    public MoneyTable registerMoney(MoneyTable fintechName) {
        return moneyRepository.save(fintechName);
    }

    @Override
    public MoneyTable updateMoney(Long id, MoneyTable newMoney) {

        return moneyRepository.findById(id).map(moneyTable -> {
//            moneyTable.setCode(moneyTable.getCode());
            moneyTable.setNameOfFintech(moneyTable.getNameOfFintech());
            moneyTable.setAmountOfMoney(moneyTable.getAmountOfMoney());
            moneyTable.setUserId(moneyTable.getUserId());
            moneyTable.setDompetId((moneyTable.getDompetId()));
            return moneyRepository.save(moneyTable);
        }).orElseGet(() -> {
            return null;
        });
    }
}




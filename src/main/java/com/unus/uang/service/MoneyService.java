package com.unus.uang.service;

import com.unus.uang.model.MoneyTable;

import java.util.List;

public interface MoneyService {

    public List<MoneyTable> findAll();

    public void deleteById(Long id);

    public MoneyTable registerMoney(MoneyTable fintechName);

    public MoneyTable updateMoney(Long id, MoneyTable newMoney);

}

package com.unus.notesb.core;

public class NoteBase {
    private long id;

    private String title;

    private String description;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.description = desc;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDesc() {
        return this.description;
    }

}

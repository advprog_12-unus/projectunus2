package com.unus.notesb.service;

import com.unus.notesb.core.NotificationBase;

import java.util.List;

public interface NotificationService {
    public void deleteNotif(Long id); //delete

    public NotificationBase register(NotificationBase notif); //create

    public List<NotificationBase> findAllNotif();
}

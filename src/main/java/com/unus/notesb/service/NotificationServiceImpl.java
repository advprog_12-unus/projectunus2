package com.unus.notesb.service;

import com.unus.notesb.core.NotificationBase;
import com.unus.notesb.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    private NotificationRepository notifRepo;

    public NotificationServiceImpl(NotificationRepository notifRepo) {
        this.notifRepo = notifRepo;
    }

    @Override
    public NotificationBase register(NotificationBase notif) {
        return notifRepo.save(notif);
    }

    @Override
    public void deleteNotif(Long id) {
        notifRepo.deleteById(id);
    }

    public List<NotificationBase> findAllNotif() {
        return notifRepo.findAll();
    }
}

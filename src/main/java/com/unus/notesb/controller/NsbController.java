package com.unus.notesb.controller;

import com.unus.account.repository.UserRepository;
import com.unus.notesb.core.NoteBase;
import com.unus.notesb.core.NotificationBase;
import com.unus.notesb.repository.NotificationRepository;
import com.unus.notesb.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class NsbController {
    @Autowired
    private NotificationService notifService;

    @Autowired
    private NotificationRepository notifRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RestTemplate restTemplate;

    public NsbController(NotificationService notificationService) {
        this.notifService = notificationService;
    }

    @GetMapping("/notes")
    private String getNote(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Long idUser = userRepo.findByUserName(auth.getName()).getId();
        model.addAttribute("notifList",
                restTemplate.getForObject(
                        "http://UNUSNOTE-SERVICE/service-notes/allNotes/" + idUser,
                        NoteBase[].class));
        return "Notes/Notes";
    }

    /** Route untuk mengisi note baru. */
    @GetMapping("/notes/create-note")
    public String createNote(Model model) {
        model.addAttribute("newNotif",
                restTemplate.getForObject("http://UNUSNOTE-SERVICE/service-notes/create-note/",
                        NoteBase.class));
        return "Notes/NoteForm";
    }

    /** Route untuk menyimpan note yang ditambahkan dan kembali ke halaman list notes. */
    @PostMapping("/notes/add-note")
    public String addNote(@ModelAttribute("notificat") NoteBase notifica) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Long idUser = userRepo.findByUserName(auth.getName()).getId();

        HttpEntity<NoteBase> request = new HttpEntity<>(notifica);
        restTemplate.postForObject("http://UNUSNOTE-SERVICE/service-notes/add-note/" + idUser,
                request, NoteBase.class);
        return "redirect:/notes";
    }

    // Route untuk membuang notifikasi yang dipilih
    @GetMapping("/notes/delete/{id}")
    public String deleteNote(@PathVariable Long id) {
        restTemplate.getForObject("http://UNUSNOTE-SERVICE/service-notes/delete-note/" + id, void.class);
        return "redirect:/notes";
    }

    // Route ke split bill
    @GetMapping("/split-bill")
    private String splitBill() {
        return "Notification/SplitBill";
    }

    // Route ke tampilan notifikasi
    @GetMapping("/note")
    private String notif(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String uname = auth.getName();
        model.addAttribute("notifList", userRepo.findByUserName(uname).getNotification());
        return "Notification/Notification";
    }

    // Route ke form notifikasi
    @GetMapping("/note/create-note")
    public String createNotif(Model model) {
        model.addAttribute("newNotif", new NotificationBase());
        return "Notification/notifForm";
    }

    /** Route untuk menyimpan notifikasi yang ditambahkan dan kembali ke halaman notifikasi. */
    @PostMapping("/note/add-note")
    public String addNotif(@ModelAttribute("notificat") NotificationBase notifica) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String uname = auth.getName();
        notifica.setUser(userRepo.findByUserName(uname));
        notifService.register(notifica);
        return "redirect:/note";
    }

    // Route untuk membuang notifikasi yang dipilih
    @GetMapping("/note/delete/{id}")
    public String deleteNotif(@PathVariable Long id) {
        notifService.deleteNotif(id);
        return "redirect:/note";
    }

}

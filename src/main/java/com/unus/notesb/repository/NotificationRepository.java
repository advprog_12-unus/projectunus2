package com.unus.notesb.repository;

import com.unus.notesb.core.NotificationBase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<NotificationBase, Long> {
}

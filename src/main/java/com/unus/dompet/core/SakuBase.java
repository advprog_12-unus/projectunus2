package com.unus.dompet.core;

public class SakuBase {
    private long id;

    private String name;

    private String desc;

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String desc) {
        this.desc = desc;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.desc;
    }

}

package com.unus.dompet.core;

import com.unus.account.model.BaseUser;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "dompet")
public class Dompet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "TEXT", name="description")
    private String desc;

    @Column(nullable = false)
    private int total;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private BaseUser user;
    
    @OneToOne(mappedBy = "dompet", cascade = CascadeType.ALL)
    public DompetState state;
    
    /**
    * Make constructor for Dompet
    */

    public Dompet(String nama, String desc) {
        this.state = new DompetLocked(this);
        this.name = nama;
        this.desc = desc;
        this.total = 0;
    }

    public Dompet(){}

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.desc = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return desc;
    }

    public int getTotal() {
        return total;
    }

    public void setUser(BaseUser user) {
        this.user = user;
    }

    public BaseUser getUser() {
        return this.user;
    }
    
    public long getId() {
        return id;
    }
}
package com.unus.dompet.core;

public class DompetUnlocked extends DompetState {
    DompetUnlocked(Dompet dompet) {
        super(dompet);
    }

    @Override
    public void changeState() {
        dompet.state = new DompetLocked(dompet);
    }

    @Override
    public String changeName(String name) {
        dompet.setName(name);
        return name;
    }
    
    @Override
    public String changeDescription(String description) {
        dompet.setDescription(description);
        return description;
    }
}

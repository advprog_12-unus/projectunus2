package com.unus.dompet.controller;

import com.unus.account.repository.UserRepository;
import com.unus.dompet.core.Dompet;
import com.unus.dompet.core.SakuBase;
import com.unus.dompet.service.DompetService;
import com.unus.dompet.repository.DompetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class DompetController {

    @Autowired
    private DompetService dompetService;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RestTemplate restTemplate;

    public DompetController(DompetService dompetService) {
        this.dompetService = dompetService;
    }

    @GetMapping("/saku")
    private String getSaku(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Long idUser = userRepo.findByUserName(auth.getName()).getId();
        model.addAttribute("dompetList", 
            restTemplate.getForObject(
                "http://UNUSSAKU-SERVICE/service-saku/allSaku/" + idUser,
                SakuBase[].class));
        return "Saku/saku";
    }

    @GetMapping("/saku/create-saku")
    public String createSaku(Model model) {
        model.addAttribute("newDompet",
                restTemplate.getForObject("http://UNUSSAKU-SERVICE/service-saku/create-saku/",
                        SakuBase.class));
        return "Saku/addsaku";
    }

    @PostMapping("/saku/add-saku")
    public String addSaku(@ModelAttribute("Saku") SakuBase notifica){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Long idUser = userRepo.findByUserName(auth.getName()).getId();

        HttpEntity<SakuBase> request = new HttpEntity<>(notifica);
        restTemplate.postForObject("http://UNUSSAKU-SERVICE/service-saku/add-saku/" + idUser,
                request, SakuBase.class);
        return "redirect:/saku";
    }

    @GetMapping("/saku/delete/{id}")
    public String deleteNote(@PathVariable Long id) {
        restTemplate.getForObject("http://UNUSSAKU-SERVICE/service-saku/delete-saku/" + id, void.class);
        return "redirect:/saku";
    }

    @GetMapping("/dompet")
    private String dompet(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String uname = auth.getName();
        model.addAttribute("dompetList", userRepo.findByUserName(uname).getDompet());
        return "Dompet/dompet";
    }

    @GetMapping("/dompet/createdompet")
    public String createDompet(Model model) {
        model.addAttribute("newDompet", new Dompet("",""));
        return "Dompet/adddompet";
    }    
     
    /**
    * Make constructor for Dompet
    */
    @PostMapping("/dompet/add-dompet")
    public String addDompet(@ModelAttribute("Dompet") Dompet dompet) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String uname = auth.getName();
        dompet.setUser(userRepo.findByUserName(uname));
        dompetService.register(dompet);
        return "redirect:/dompet";
    }

    @GetMapping("dompet/delete/{id}")
    public String deleteDompet(@PathVariable Long id) {
        dompetService.deleteDompet(id);
        return "redirect:/dompet";
    }
}

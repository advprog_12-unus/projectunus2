package com.unus.dompet.service;

import com.unus.dompet.core.Dompet;

import java.util.List;

public interface DompetService {
    public void deleteDompet(Long id); 

    public Dompet register(Dompet dompet);

    public List<Dompet> findAlldompet();

    public Dompet updateDompet(Long id,Dompet newDompet);
}

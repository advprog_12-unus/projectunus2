package com.unus.account.repository;

import com.unus.account.model.BaseUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<BaseUser, Long> {
    BaseUser findByUserName(String userName);
}
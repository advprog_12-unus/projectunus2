package com.unus.account.service.error;

public final class UsernameAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = -3598877005093351990L;
    
    public UsernameAlreadyExistException(final String message) {
        super(message);
    }
}
package com.unus.account.service;

import com.unus.account.model.BaseUser;
import com.unus.account.model.UserDtoBase;
import com.unus.account.service.error.UsernameAlreadyExistException;

public interface UserService {

    public BaseUser registerUser(UserDtoBase userDto) throws UsernameAlreadyExistException;

    public Boolean usernameExists(final String email);
    
    public BaseUser saveUser(BaseUser user);
}
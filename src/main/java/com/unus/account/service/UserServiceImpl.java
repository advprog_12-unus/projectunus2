package com.unus.account.service;

import com.unus.account.model.BaseUser;
import com.unus.account.model.UserDtoBase;
import com.unus.account.repository.UserRepository;
import com.unus.account.service.error.UsernameAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public BaseUser registerUser(UserDtoBase userDto) throws UsernameAlreadyExistException {
        if (usernameExists(userDto.getUsername())) {
            throw new UsernameAlreadyExistException(
                "UserAlreadyExists"
            );
        }
        BaseUser user = new BaseUser(
            userDto.getUsername(),
            passwordEncoder.encode(userDto.getPassword()),
            "USER"
        );
        return saveUser(user);
    }

    @Override
    public Boolean usernameExists(String username) {
        return this.userRepository.findByUserName(username) != null;
    }

    @Override
    public BaseUser saveUser(BaseUser user) {
        return this.userRepository.save(user);
    }
}
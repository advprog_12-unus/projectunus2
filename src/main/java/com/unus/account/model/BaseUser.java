package com.unus.account.model;

import com.unus.dompet.core.Dompet;

import com.unus.notesb.core.NotificationBase;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "unususer")
public class BaseUser implements User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String userName;

    @Column(nullable = false)
    private String password;

    private boolean active;

    private String roles = "";

    @OneToMany(mappedBy = "unususer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<NotificationBase> notificationBaseSet;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Dompet> dompet;

    /**
     * Base User Constructor.
     */
    public BaseUser(String userName, String password, String roles) {
        this.userName = userName;
        this.password = password;
        this.active = true;
        this.roles = roles;
    }

    protected BaseUser() {
    }

    public long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public boolean isActive() {
        return active;
    }

    public String getRoles() {
        return roles;
    }

    /**
     * Get roles of a certain user.
     */
    public List<String> getRolesList() {
        if (roles.length() > 0) {
            return Arrays.asList(roles.split(","));
        }
        return new ArrayList<>();
    }

    public Set<NotificationBase> getNotification() {
        return this.notificationBaseSet;
    }

    public Set<Dompet> getDompet() {
        return this.dompet;
    }

    /**
     * Get a dompet based on user id.
     * @param id the id used to search dompet
     * @return dompet object
     */
    public Dompet getDompetWithID(Long id) {
        for (Dompet d: dompet) {
            if (d.getId() == id) {
                return d;
            }
        }
        return null;
    }
}

package com.unus.account.model;

public interface UserDto {

    public String getUsername();

    public String getPassword();

    public String getMatchingPassword();
}
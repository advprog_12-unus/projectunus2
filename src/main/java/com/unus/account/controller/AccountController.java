package com.unus.account.controller;

import com.unus.account.model.UserDtoBase;
import com.unus.account.service.UserServiceImpl;
import com.unus.account.service.error.UsernameAlreadyExistException;
import com.unus.account.util.GenericResponse;
import com.unus.account.util.StringRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AccountController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping("/register")
    public String registrationForm() {
        return "User/registerAjax";
    }

    /**
     * Save user to database.
     * @param userDto JSON that will map to this object
     * @return JSON message
     */
    @PostMapping(value = "/register")
    public @ResponseBody ResponseEntity registerUser(@RequestBody UserDtoBase userDto) {
        try {
            userService.registerUser(userDto);
        } catch (UsernameAlreadyExistException uaeEx) {
            return ResponseEntity.ok(
                    new GenericResponse("Username already exist.", "UserAlreadyExist"));
        }
        return ResponseEntity.ok(new GenericResponse("success"));
    }

    /**
     * Check username if exist in database or not.
     * @param username JSON from ajax post request that will map to this object
     * @return JSON message
     */
    @PostMapping(value = "/checkUsername")
    public @ResponseBody ResponseEntity checkUsername(@RequestBody StringRequestBody username) {
        if (userService.usernameExists(username.getUsername())) {
            return ResponseEntity.ok(
                    new GenericResponse("Username already exist.", "UserAlreadyExist"));
        }
        return ResponseEntity.ok(new GenericResponse("Username available.", "notExist"));
    }
}

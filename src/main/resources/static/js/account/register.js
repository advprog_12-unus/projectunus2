let usernameCheck = false;
let passwordCheck = false;

$(document).ready(function () {
    $('#submit-button').attr('disabled', true);

    $('#username').change(function (event) {
        checkUser(event);
    });

    $('#password').change(function (event) {
        if ($('#password').val() != "") {
            $('#password').addClass("is-valid");
            $('#password').removeClass("is-invalid");
        } else {
            $('#password').addClass("is-invalid");
            $('#password').removeClass("is-valid");
        }
    })

    $('#matchingPassword').keyup(function (event) {
        checkPasswordMatch(event);
    });

    $('form').submit(function(event) {
        if (usernameCheck && passwordCheck) {
            register(event);
        } else {
            message = "<h1 style='color: red;'>Harap lengkapi form.</h1>"
            $('#feedback').html(message);
        }
    });
});

let token = $("meta[name='_csrf']").attr("content");
let header = $("meta[name='_csrf_header']").attr("content");
$(document).ajaxSend(function(e, xhr, options) {
    xhr.setRequestHeader(header, token);
});

function display(data) {
    let json = "<h4>Ajax Response</h4><pre>"
        + JSON.stringify(data, null, 4) + "</pre>";
    $('#feedback').html(json);
}

function checkUser(event) {
    event.preventDefault();
    $(".alert").html("").hide();
    $('#usernameError').html("").hide()

    let username = {};
    username['username'] = $("#username").val();

    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/checkUsername",
        data : JSON.stringify(username),
        dataType: 'json',
        success : function (data) {
            // display(data);
            if (data.error === "UserAlreadyExist") {
                $("#usernameError").show().html(data.message);
                usernameCheck = false;
                $('#username').addClass("is-invalid");
                $('#username').removeClass("is-valid");
            } else {
                usernameCheck = true;
                $('#username').addClass("is-valid");
                $('#username').removeClass("is-invalid");
                $('#usernameValid').show().html(data.message)
                
            }
            if (usernameCheck && passwordCheck) {
                $('#submit-button').attr('disabled', false);
            } else {
                $('#submit-button').attr('disabled', true);
            }
        },
        error : function(e) {
            display(e);
        }
    });
}

function checkPasswordMatch(event) {
    $(".alert").html("").hide();
    let password = $('#password').val();
    let matchingPassword = $('#matchingPassword').val()

    if (password != matchingPassword) {
        passwordCheck = false;
        $("#matchingPasswordError").show().html("Password not match.");
        $('#matchingPassword').addClass("is-invalid");
        $('#matchingPassword').removeClass("is-valid");
    } else {
        passwordCheck = true;
        $('#matchingPassword').addClass("is-valid");
        $('#matchingPassword').removeClass("is-invalid");
    }

    if (usernameCheck && passwordCheck) {
        $('#submit-button').attr('disabled', false);
    } else {
        $('#submit-button').attr('disabled', true);
    }
}

function register(event) {
    event.preventDefault();
    $(".alert").html("").hide();

    let userDto = {};
    userDto['username'] = $("#username").val();
    userDto['password'] = $("#password").val();
    userDto['matchingPassword'] = $("#matchingPassword").val();

    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/register",
        data : JSON.stringify(userDto),
        dataType : 'json',
        success : function (data) {
            console.log("data : " + data);
            // display(data);
            if (data.message === "success") {
                let response = '<span class="alert alert-success col-sm-4">'
                                + '<span>Registrasi berhasil.</span>'
                                + '</span>'
                $('#feedback').html(response);
            }
            if (data.error === "UserAlreadyExists") {
                $("#usernameError").show().html(data.message);
            }
        },
        error : function (e) {
            console.log("Error: ", e);
            display(e);
        }
    });
}

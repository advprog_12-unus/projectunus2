package com.unus.uangtest.model;

import com.unus.uang.model.MoneyTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MoneyTableTest {

    @BeforeEach
    void setUp(){
        MoneyTable moneyTable = new MoneyTable("CASH", 5000, 1, 1);
    }

    @Test
    void objMoneyTableNullTest() {
        MoneyTable moneyTableNoParameter = new MoneyTable();

        assertNull(moneyTableNoParameter.getCode());
        assertNull(moneyTableNoParameter.getNameOfFintech());
    }

    @Test
    void objMoneyTableTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 10000, 1, 1);

        assertEquals("000", moneyTable.getCode());
        assertEquals("CASH", moneyTable.getNameOfFintech());
        assertEquals(10000, moneyTable.getAmountOfMoney());
        assertEquals(1, moneyTable.getUserId());
        assertEquals(1, moneyTable.getDompetId());
    }

    @Test
    void objMoneyTableSetCodeTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setCode("001");
        assertEquals("001", moneyTable.getCode());
    }

    @Test
    void objMoneyTableSetNameOfFintechTest(){
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setNameOfFintech("GOPAY");
        assertEquals("GOPAY", moneyTable.getNameOfFintech());
    }

    @Test
    void objMoneyTableSetAmoutOfMoneyTest(){
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setAmountOfMoney(100000);

        assertEquals(100000, moneyTable.getAmountOfMoney());
    }

    @Test
    void objMoneyTableSetUserIdTest(){
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1 ,1);

        moneyTable.setUserId(2);

        assertEquals(2, moneyTable.getUserId());
    }

    @Test
    void objMoneyTableSetDompetIdTest(){
        MoneyTable moneyTable = new MoneyTable("CASH", 50000,1 ,1);

        moneyTable.setDompetId(2);

        assertEquals(2, moneyTable.getDompetId());
    }

    @Test
    void getCodeTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);
        assertEquals("000", moneyTable.getCode());
    }

    @Test
    void setCodeTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setCode("123");
        assertEquals("123", moneyTable.getCode());
        moneyTable.setCode("000");
    }

    @Test
    void getNameOfFintechTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        assertEquals("CASH", moneyTable.getNameOfFintech());
    }

    @Test
    void setNameOfFintechTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setNameOfFintech("OVO");
        assertEquals("OVO", moneyTable.getNameOfFintech());
        moneyTable.setNameOfFintech("CASH");
    }

    @Test
    void getAmoutOfMoneyTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        assertEquals(50000, moneyTable.getAmountOfMoney());
    }

    @Test
    void setAmoutOfMoneyTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setAmountOfMoney(700000);
        assertEquals(700000, moneyTable.getAmountOfMoney());
        moneyTable.setAmountOfMoney(50000);
    }

    @Test
    void getIdTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        assertEquals(0, moneyTable.getId());
    }

    @Test
    void setIdTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setId(12345);
        assertEquals(12345, moneyTable.getId());
        moneyTable.setId(11111);
    }

    @Test
    void getUserIdTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        assertEquals(1, moneyTable.getUserId());
    }

    @Test
    void setUserIdTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setUserId(2);
        assertEquals(2, moneyTable.getUserId());
        moneyTable.setUserId(1);
    }

    @Test
    void getDompetIdTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        assertEquals(1, moneyTable.getDompetId());
    }

    @Test
    void setDompetIdTest() {
        MoneyTable moneyTable = new MoneyTable("CASH", 50000, 1, 1);

        moneyTable.setDompetId(2);
        assertEquals(2, moneyTable.getDompetId());
        moneyTable.setDompetId(1);
    }
}

package com.unus.uangtest.model;

import com.unus.notesb.core.NoteBase;
import com.unus.uang.model.MoneyTable;
import com.unus.uang.model.UangBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UangBaseTest {

    private UangBase uangBase;

    @BeforeEach
    void setUp(){
        uangBase = new UangBase();
        uangBase.setCode("000");
        uangBase.setNameOfFintech(("CASH"));
        uangBase.setAmountOfMoney(50000);
        uangBase.setId(1);
        uangBase.setDompetId(1);
        uangBase.setUserId(1);
    }

    @Test
    void objUangBaseNullTest() {
        UangBase uangBaseNoParameter = new UangBase();

        assertNull(uangBaseNoParameter.getCode());
        assertNull(uangBaseNoParameter.getNameOfFintech());
    }

    @Test
    void getCodeUangTest() {
        assertEquals("000", uangBase.getCode());
    }

    @Test
    void setCodeUangTest() {
        uangBase.setCode("123");
        assertEquals("123", uangBase.getCode());
        uangBase.setCode("000");
    }

    @Test
    void getNameOfFintechTest() {
        assertEquals("CASH", uangBase.getNameOfFintech());
    }

    @Test
    void setNameOfFintechTest() {
        uangBase.setNameOfFintech("OVO");
        assertEquals("OVO", uangBase.getNameOfFintech());
        uangBase.setNameOfFintech("CASH");
    }

    @Test
    void getAmountOfMoneyUangTest() {
        assertEquals(50000, uangBase.getAmountOfMoney());
    }

    @Test
    void setAmountOfMoneyUangTest() {
        uangBase.setAmountOfMoney(1000);
        assertEquals(1000, uangBase.getAmountOfMoney());
        uangBase.setAmountOfMoney(50000);
    }

    @Test
    void getIdUangTest() {
        assertEquals(1, uangBase.getId());
    }

    @Test
    void setIdUangTest() {
        uangBase.setId(10);
        assertEquals(10, uangBase.getId());
        uangBase.setId(1);
    }

    @Test
    void getDompetTest() {
        assertEquals(1, uangBase.getDompetId());
    }

    @Test
    void setDompetTest() {
        uangBase.setDompetId(10);
        assertEquals(10, uangBase.getDompetId());
        uangBase.setDompetId(1);
    }

    @Test
    void getUserTest() {
        assertEquals(1, uangBase.getDompetId());
    }

    @Test
    void setUserTest() {
        uangBase.setUserId(10);
        assertEquals(10, uangBase.getUserId());
        uangBase.setUserId(1);
    }
}

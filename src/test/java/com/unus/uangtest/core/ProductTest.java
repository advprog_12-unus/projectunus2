package com.unus.uangtest.core;

import com.unus.uang.core.MoneyFactory;
import com.unus.uang.core.Product.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ProductTest {

    @Test
    void fintechCashTest(){
        FintechCash fintechCash = new FintechCash();

        assertEquals("000", fintechCash.getCode());
        assertEquals("CASH", fintechCash.getNameFintech());
        assertEquals(0, fintechCash.getAmountMoney());
    }

    @Test
    void fintechDanaTest(){
        FintechDana fintechDana = new FintechDana();

        assertEquals("001", fintechDana.getCode());
        assertEquals("DANA", fintechDana.getNameFintech());
        assertEquals(0, fintechDana.getAmountMoney());
    }

    @Test
    void fintechGopayTest(){
        FintechGopay fintechGopay = new FintechGopay();

        assertEquals("002", fintechGopay.getCode());
        assertEquals("GOPAY", fintechGopay.getNameFintech());
        assertEquals(0, fintechGopay.getAmountMoney());
    }

    @Test
    void fintechOvoTest(){
        FintechOvo fintechOvo = new FintechOvo();

        assertEquals("003", fintechOvo.getCode());
        assertEquals("OVO", fintechOvo.getNameFintech());
        assertEquals(0, fintechOvo.getAmountMoney());
    }

    @Test
    void moneyFactory1Test(){
        MoneyFactory moneyFactory = new MoneyFactory();

        Money money1 = moneyFactory.getMoney("CASH");

        assertEquals("Create Cash", money1.createMoney());
    }

    @Test
    void moneyFactory2Test(){
        MoneyFactory moneyFactory = new MoneyFactory();

        Money money2 = moneyFactory.getMoney("DANA");

        assertEquals("Create Dana", money2.createMoney());
    }

    @Test
    void moneyFactory3Test(){
        MoneyFactory moneyFactory = new MoneyFactory();

        Money money3 = moneyFactory.getMoney("GOPAY");

        assertEquals("Create Gopay", money3.createMoney());
    }

    @Test
    void moneyFactory4Test(){
        MoneyFactory moneyFactory = new MoneyFactory();

        Money money4 = moneyFactory.getMoney("OVO");

        assertEquals("Create Ovo", money4.createMoney());
    }

    @Test
    void moneyFactory5Test(){
        MoneyFactory moneyFactory = new MoneyFactory();

        Money money5 = moneyFactory.getMoney("MANDIRI");

        assertNull(money5);
    }

    @Test
    void moneyFactory6Test(){
        MoneyFactory moneyFactory = new MoneyFactory();

        Money money6 = moneyFactory.getMoney("");

        assertNull(money6);
    }
}

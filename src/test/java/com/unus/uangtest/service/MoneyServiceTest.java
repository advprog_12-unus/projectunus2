package com.unus.uangtest.service;

import com.unus.uang.model.MoneyTable;
import com.unus.uang.repository.MoneyRepository;
import com.unus.uang.service.MoneyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class MoneyServiceTest {

    private MoneyTable moneyTable;

    @Mock
    private MoneyRepository moneyRepository;

    @InjectMocks
    private MoneyServiceImpl moneyService;

    @BeforeEach
    void setUp(){
        MoneyTable moneyTable = new MoneyTable("CASH", 5000, 1, 1);
    }

    @Test
    void deleteTest(){

        MoneyTable moneyTable1 = new MoneyTable("CASH", 5000, 1, 1);

        moneyService.registerMoney(moneyTable1);
        moneyService.deleteById(moneyTable1.getId());
        lenient().when(moneyService.registerMoney(moneyTable1)).thenReturn(moneyTable1);
    }

    @Test
    void findAllTest(){
        List<MoneyTable> moneyTableList = moneyService.findAll();
        lenient().when(moneyService.findAll()).thenReturn(moneyTableList);
    }

    @Test
    void registerMoneyTest(){
        moneyService.registerMoney(moneyTable);
        lenient().when(moneyService.registerMoney(moneyTable)).thenReturn(moneyTable);
    }

    @Test
    void updateMoney() {
        Long i = (long) 0;

        MoneyTable moneyTable1 = new MoneyTable("CASH", 5000, 1, 1);
        moneyTable1.setId(0);

        MoneyTable moneyTable2 = new MoneyTable("DANA", 5000, 1, 1);

        MoneyTable moneyBaru = moneyService.updateMoney(i, moneyTable2);
        lenient().when(moneyService.registerMoney(moneyBaru)).thenReturn(moneyBaru);
    }
}

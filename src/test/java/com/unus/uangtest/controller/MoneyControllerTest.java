package com.unus.uangtest.controller;

import com.unus.account.model.BaseUser;
import com.unus.account.repository.UserRepository;
import com.unus.account.security.UserDetailsImpl;
import com.unus.account.security.UserDetailsServiceImpl;
import com.unus.dompet.repository.DompetRepository;
import com.unus.dompet.service.DompetServiceImpl;
import com.unus.uang.controller.MoneyController;
import com.unus.uang.model.UangBase;
import com.unus.uang.repository.MoneyRepository;
import com.unus.uang.service.MoneyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@WebMvcTest(controllers = MoneyController.class)
public class MoneyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RestTemplate restTemplate;

    @MockBean
    private UserDetailsImpl userDetails;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private BaseUser user;

    @MockBean
    private DompetServiceImpl dompetService;

    @MockBean
    private DompetRepository dompetRepository;

    @MockBean
    private MoneyServiceImpl moneyService;

    @MockBean
    private MoneyRepository moneyRepository;

    @BeforeEach
    public void setUp() throws Exception {
        user = new BaseUser("admin", "admin123", "USER");
        userDetails = new UserDetailsImpl(user);
    }

//    @Test
//    public void uangsUrlWithoutAuthTest() throws Exception {
////        mockMvc.perform(get("/uangs/1"))
////                .andExpect(status().is3xxRedirection());
//
//        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//        restTemplateBuilder.configure(restTemplate);
//        TestRestTemplate testRestTemplate = new TestRestTemplate(restTemplateBuilder);
//        ResponseEntity<UangBase[]> response = testRestTemplate.getForEntity(
//                "http://UNUSUANG-SERVICE/service-uangs/create-uang/" + "/1/1", UangBase[].class);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//    }
//
//    @Test
//    public void createUangsUrlWithoutAuthTest() throws Exception {
//        mockMvc.perform(get("/uangs/create-uang"))
//                .andExpect(status().is3xxRedirection());
//    }



}

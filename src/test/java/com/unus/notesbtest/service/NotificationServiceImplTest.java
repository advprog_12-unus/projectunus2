package com.unus.notesbtest.service;

import com.unus.notesb.core.NotificationBase;
import com.unus.notesb.repository.NotificationRepository;
import com.unus.notesb.service.NotificationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class NotificationServiceImplTest {
    @Mock
    private NotificationRepository notifRepo;

    private NotificationBase notifTest;

    @InjectMocks
    private NotificationServiceImpl notifService;

    @BeforeEach
    void setUp() {
        notifTest = new NotificationBase();
        notifTest.setTitle("TestingTitle");
        notifTest.setDesc("TestingDescription");
    }

    @Test
    void registerTest() {
        notifService.register(notifTest);
        lenient().when(notifService.register(notifTest)).thenReturn(notifTest);
    }

    @Test
    void deleteTest() {
        notifService.register(notifTest);
        notifService.deleteNotif(notifTest.getId());
        lenient().when(notifService.register(notifTest)).thenReturn(notifTest);
    }

    @Test
    void findAllNotifTest() {
        List<NotificationBase> notifList = notifService.findAllNotif();
        lenient().when(notifService.findAllNotif()).thenReturn(notifList);
    }
}
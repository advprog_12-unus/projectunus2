package com.unus.notesbtest.core;

import com.unus.notesb.core.NoteBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class NoteBaseTest {
    private NoteBase noteTest;

    @BeforeEach
    void setUp() {
        noteTest = new NoteBase();
        noteTest.setTitle("TitleTest");
        noteTest.setDesc("DescTest");
    }

    @Test
    void setTitleTest() {
        noteTest.setTitle("Title2");
        assertEquals("Title2", noteTest.getTitle());
        noteTest.setTitle("TitleTest");
    }

    @Test
    void setDescTest() {
        noteTest.setDesc("Desc2");
        assertEquals("Desc2", noteTest.getDesc());
        noteTest.setDesc("DescTest");
    }

    @Test
    void getIdTest() {
        assertEquals(0, noteTest.getId());
    }

    @Test
    void getTitleTest() {
        assertEquals("TitleTest", noteTest.getTitle());
    }

    @Test
    void getDescTest() {
        assertEquals("DescTest", noteTest.getDesc());
    }

}
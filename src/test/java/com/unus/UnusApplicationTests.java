package com.unus;

import com.unus.account.security.UserDetailsImpl;
import com.unus.account.security.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class UnusApplicationTests {

    @MockBean
    private UserDetailsImpl userDetails;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @Test
    void contextLoads() {
    }

    @Test
    public void mainFunctionTest() {
        UnusApplication.main(new String[] {});
    }
}
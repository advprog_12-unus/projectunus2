package com.unus.accounttest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unus.account.controller.AccountController;
import com.unus.account.model.BaseUser;
import com.unus.account.model.UserDtoBase;
import com.unus.account.repository.UserRepository;
import com.unus.account.security.UserDetailsImpl;
import com.unus.account.security.UserDetailsServiceImpl;
import com.unus.account.service.UserServiceImpl;

import com.unus.account.util.StringRequestBody;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.servlet.http.HttpServletRequest;

@WebMvcTest(controllers = AccountController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private HttpServletRequest request;

    @MockBean
    private UserServiceImpl userService;

    @MockBean
    private UserDetailsImpl userDetails;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private BaseUser user;

    @MockBean
    private UserDtoBase userDto;

    @Test
    public void getRegisterTest() throws Exception {
        mockMvc
                .perform(get("/register"))
                .andExpect(status().isOk())
                .andExpect(view().name("User/registerAjax"));
    }

    @Test
    public void postCheckUserTest() throws Exception {
        StringRequestBody stringRequestBody = new StringRequestBody();
        stringRequestBody.setUsername("test");
        mockMvc.
                perform(post("/checkUsername")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(stringRequestBody)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void postRegisterTest() throws Exception {
        UserDtoBase userDto = new UserDtoBase();
        userDto.setUsername("test");
        userDto.setPassword("pass");
        userDto.setMatchingPassword("pass");
        mockMvc.
                perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userDto)))
                .andExpect(status().is4xxClientError());
    }
}
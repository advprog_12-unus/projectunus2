package com.unus.accounttest.securityservice;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.unus.account.repository.UserRepository;
import com.unus.account.security.UserDetailsServiceImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceImplTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    @Test
    @WithMockUser(username = "admin", roles = {"ADMIN"})
    public void loadUserByUsernameShouldCallUserRepositoryFindUserByName(){
        UserDetails user = userDetailsService.loadUserByUsername("admin");
        verify(userRepository, times(1)).findByUserName("admin");
        assertNotNull(user);
    }
}

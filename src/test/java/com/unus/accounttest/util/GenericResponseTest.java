package com.unus.accounttest.util;

import com.unus.account.model.BaseUser;
import com.unus.account.util.GenericResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GenericResponseTest {
    GenericResponse response;
    GenericResponse responseWithError;

    @BeforeEach
    public void setUp() throws Exception {
        response = new GenericResponse("message");
        responseWithError = new GenericResponse("errorMessage", "error");
    }

    @Test
    public void getMessageShouldReturnMessage() {
        assertEquals("message", response.getMessage());
        assertEquals("errorMessage", responseWithError.getMessage());
    }

    @Test
    public void getErrorShouldReturnError() {
        assertNull(response.getError());
        assertEquals("error", responseWithError.getError());
    }

    @Test
    public void setErrorShouldChangeError() {
        responseWithError.setError("errorMessage2");
        assertEquals("errorMessage2", responseWithError.getError());
    }

    @Test
    public void setMessageShouldChangeMessage() {
        response.setMessage("message2");
        assertEquals("message2", response.getMessage());
    }
}

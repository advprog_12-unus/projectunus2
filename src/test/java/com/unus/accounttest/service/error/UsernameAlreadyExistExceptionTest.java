package com.unus.accounttest.service.error;

import com.unus.account.service.error.UsernameAlreadyExistException;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;

public class UsernameAlreadyExistExceptionTest {
    
    @Test
    public void testException() {
        UsernameAlreadyExistException exception = catchThrowableOfType(
                () -> {throw new UsernameAlreadyExistException("Username already exist.");},
                UsernameAlreadyExistException.class
        );

        assertThat(exception).hasMessageContaining("exist");

        assertThat(catchThrowableOfType(() -> {}, Exception.class)).isNull();
    }
}
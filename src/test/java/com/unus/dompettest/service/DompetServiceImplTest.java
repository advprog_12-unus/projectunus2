package com.unus.dompet.service;

import com.unus.dompet.core.Dompet;
import com.unus.dompet.repository.DompetRepository;
import com.unus.dompet.service.DompetServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class DompetServiceImplTest {
    @Mock
    private DompetRepository dompetRepo;

    private Dompet dompet;

    @InjectMocks
    private DompetServiceImpl dompetService;

    @BeforeEach
    void setUp() {
        Dompet dompet = new Dompet("At","Uang kas ate");
    }

    @Test
    void registerTest() {
        dompetService.register(dompet);
        lenient().when(dompetService.register(dompet)).thenReturn(dompet);
    }

    @Test
    void deletedompetTest() {
        Dompet dompet = new Dompet("At","Uang kas ate");

        dompetService.register(dompet);
        dompetService.deleteDompet(dompet.getId());
        lenient().when(dompetService.register(dompet)).thenReturn(dompet);
    }

    @Test
    void findAllDompetTest() {
        List<Dompet> dompetList = dompetService.findAlldompet();
        lenient().when(dompetService.findAlldompet()).thenReturn(dompetList);
    }

    @Test
    void updateDompet() {
        Long a = (long) 1;
        Dompet dompet = new Dompet("At","Uang kas ate");
        Dompet dompetnew = dompetService.updateDompet(a, dompet);
        lenient().when(dompetService.register(dompet)).thenReturn(dompet);
    }
}
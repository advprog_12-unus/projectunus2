package com.unus.dompettest.core;
import org.junit.jupiter.api.BeforeEach;

import com.unus.account.model.BaseUser;
import com.unus.dompet.core.SakuBase;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class Sakutest {

    private SakuBase saku;
    private BaseUser userTest;

    @BeforeEach
    void setUp() {
        saku = new SakuBase();
        saku.setName("Saku");
        saku.setDescription("DescTest");
    }

    @Test
    void testobject1() {
        assertEquals("Saku", saku.getName());
    }

    @Test
    void testobject2() {
        assertEquals("DescTest", saku.getDescription());
    }

    @Test
    void getIdTest() {
        assertEquals(0, saku.getId());
    }

    @Test
    void getNameTest() {
        assertEquals("Saku", saku.getName());
    }

    @Test
    void getDescTest() {
        assertEquals("DescTest", saku.getDescription());
    }
}